import random
import simcore
import params
import logdata
import itertools


class Event:
    def __init__(self, core, timestamp):
        self.core = core
        self.timestamp = timestamp

    def run(self):
        pass


class MessageEvent(Event):
    def __init__(self, core, timestamp, src_node_id, dest_node_id):
        super().__init__(core, timestamp)
        self.src_node_id = src_node_id
        self.dest_node_id = dest_node_id
        self.dest_node = None

    def run(self):
        if self.core.node_map.node_exists(self.dest_node_id):
            self.dest_node = self.core.node_map.get_node(self.dest_node_id)


class AddNodeEvent(Event):
    def __init__(self, core, timestamp, churn_log_node_id):
        super().__init__(core, timestamp)
        self.churn_log_node_id = churn_log_node_id

    def run(self):
        new_node = self.core.node_map.create_node(self.churn_log_node_id)
        new_node.create_outgoing_connections()


class DeleteNodeEvent(Event):
    def __init__(self, core, timestamp, node_id):
        super().__init__(core, timestamp)
        self.node_id = node_id

    def run(self):
        node = self.core.node_map.get_node(self.node_id)

        # incoming nodes try to reconnect to another node after some delay
        for incoming_node_id in node.incoming:
            self.core.event_queue.create_event(
                ReconnectOutgoingEvent,
                random.randint(params.reconnect_delay[0], params.reconnect_delay[1]),
                incoming_node_id
            )

        self.core.node_map.delete_node(node)


class ReconnectOutgoingEvent(Event):
    """
    This event is scheduled when an outgoing connection of a node is closed
    That is, after the node has noticed the connection is lost, and a new
    node to connect to has been found
    """
    def __init__(self, core, timestamp, node_id):
        super().__init__(core, timestamp)
        self.node_id = node_id

    def run(self):
        if self.core.node_map.node_exists(self.node_id):
            node = self.core.node_map.get_node(self.node_id)
            node.reconnect_outgoing()


class CreateNodesEvent(Event):
    """
    Event for creating and connecting nodes
    We assume here that every node has Node.max_outgoing connections at beginning
    """
    def __init__(self, core, timestamp, churn_log_open_node_ids):
        super().__init__(core, timestamp)
        self.churn_log_open_node_ids = churn_log_open_node_ids

    def run(self):
        first_new_node_id = self.core.node_map.next_node_id
        last_new_node_id = first_new_node_id + len(self.churn_log_open_node_ids) - 1
        n_created = 0
        queued_incoming_conn = {}  # used when connto node does not exit yet
        churn_log_node_ids_iter = itertools.chain(self.churn_log_open_node_ids)

        while n_created < len(self.churn_log_open_node_ids):
            new_node = self.core.node_map.create_node(next(churn_log_node_ids_iter))
            assert new_node.id == first_new_node_id + n_created
            n_created += 1

            if new_node.id in queued_incoming_conn:
                new_node.incoming = queued_incoming_conn[new_node.id]

            n_connected = 0
            while n_connected < params.node_max_outgoing:
                # don't use NodeMap.get_random_node here because nodes might not exist yet
                connto = random.randint(1, last_new_node_id)

                if connto < first_new_node_id and (not self.core.node_map.node_exists(connto)):
                    continue  # node does not exist and will not be created

                if connto is new_node.id:
                    continue

                if connto in new_node.outgoing.union(new_node.incoming):
                    continue

                new_node.outgoing.add(connto)
                if self.core.node_map.node_exists(connto):
                    self.core.node_map.get_node(connto).incoming.add(new_node.id)
                else:
                    if connto not in queued_incoming_conn:
                        queued_incoming_conn[connto] = set()
                    queued_incoming_conn[connto].add(new_node.id)
                n_connected += 1

        if self.core.monitor_node is not None:
            self.core.monitor_node.create_outgoing_connections()


class TxReceivedEvent(MessageEvent):
    def __init__(self, core, timestamp, src_node_id, dest_node_id, inv_hash):
        super().__init__(core, timestamp, src_node_id, dest_node_id)
        self.inv_hash = inv_hash

    def run(self):
        super().run()
        if self.dest_node is None:
            return
        self.dest_node.tx_message_received(self.timestamp, self.src_node_id, self.inv_hash)


class GetDataReceivedEvent(MessageEvent):
    def __init__(self, core, timestamp, src_node_id, dest_node_id, inv_list):
        super().__init__(core, timestamp, src_node_id, dest_node_id)
        self.inv_list = inv_list

    def run(self):
        super().run()
        if self.dest_node is None:
            return
        self.dest_node.getdata_message_received(self.timestamp, self.src_node_id, self.inv_list)


class InvReceivedEvent(MessageEvent):
    def __init__(self, core, timestamp, src_node_id, dest_node_id, inv_list):
        super().__init__(core, timestamp, src_node_id, dest_node_id)
        self.inv_list = inv_list

    def run(self):
        super().run()
        if self.dest_node is None:
            return
        self.dest_node.inv_message_received(self.timestamp, self.src_node_id, self.inv_list)


class GenNewTransactionEvent(Event):
    def __init__(self, core, timestamp):
        super().__init__(core, timestamp)

    def run(self):
        src_node = self.core.node_map.get_random_node()
        new_transaction_hash = self.core.create_new_inv_hash()
        src_node.add_transaction(new_transaction_hash)
        src_node.relay_transaction(new_transaction_hash)

        self.core.event_queue.create_event(
            GenNewTransactionEvent,
            logdata.InvData.get_random_new_transaction_delay()
        )


class ThreadMessageHandlerLoopEvent(Event):
    def __init__(self, core, timestamp, node_id):
        super().__init__(core, timestamp)
        self.node_id = node_id

    def run(self):
        if not self.core.node_map.node_exists(self.node_id):
            return

        src_node = self.core.node_map.get_node(self.node_id)
        connected_node_ids = src_node.incoming.union(src_node.outgoing)

        if len(connected_node_ids) == 0:
            self.reschedule_event()
            return

        # exit early to increase performance; nothing to do if no invs to send exist
        if len(src_node.inv_to_send) == 0:
            self.reschedule_event()
            return

        trickle_node_id = random.choice(list(connected_node_ids))
        for connected_node_id in connected_node_ids:
            self.send_inv_messages(src_node, connected_node_id, trickle_node_id)

        self.reschedule_event()

    def reschedule_event(self):
        self.core.event_queue.create_event(
            ThreadMessageHandlerLoopEvent,
            params.thread_message_handler_delay,
            self.node_id
        )

    @classmethod
    def send_inv_messages(cls, src_node, dest_node_id, trickle_node_id):
        """
        Represents inventory-Message sending part of main.cpp:SendMessage
        of original Bitcoin client
        """
        if dest_node_id not in src_node.inv_to_send:
            return

        inv_to_send = src_node.inv_to_send[dest_node_id]
        inv_list_send = []
        inv_list_wait = []

        for inv_elem in inv_to_send:
            if src_node.peer_is_inv_known(dest_node_id, inv_elem.inv_hash):
                continue
            
            if inv_elem.inv_type == simcore.InvType.tx and dest_node_id != trickle_node_id:
                # send directly with p=1/4, otherwise trickle
                trickle_transaction = (src_node.inv_hash_salt ^ inv_elem.inv_hash) & 3 != 0
                if trickle_transaction:
                    inv_list_wait.append(inv_elem)
                    continue

            src_node.peer_add_inv_known(dest_node_id, inv_elem.inv_hash)
            inv_list_send.append(inv_elem)

            if len(inv_list_send) >= 1000:
                src_node.send_message(dest_node_id, simcore.Message.inv, inv_list_send)
                inv_list_send.clear()

        if len(inv_list_wait) > 0:
            src_node.inv_to_send[dest_node_id] = inv_list_wait
        else:
            del src_node.inv_to_send[dest_node_id]

        if len(inv_list_send) > 0:
            src_node.send_message(dest_node_id, simcore.Message.inv, inv_list_send)