#!/usr/bin/python3

import sys
import simcore
import logdata
import events
import time


def main(argv):
    if len(argv) != 4:
        print("usage: {:s} <churn.log> <latency.log> <inv.log>".format(argv[0]))
        return 1

    churn_log_path = argv[1]
    latency_log_path = argv[2]
    inv_log_path = argv[3]

    print("Processing log files")
    logdata.ChurnData.process_log(churn_log_path)
    logdata.LatencyData.process_log(latency_log_path)
    logdata.InvData.process_log(inv_log_path)

    core = simcore.Core()

    print("Creating initial events")
    core.event_queue.create_event(events.CreateNodesEvent, 0, logdata.ChurnData.open_node_ids_sim_init)
    core.event_queue.create_event(events.GenNewTransactionEvent, logdata.InvData.get_random_new_transaction_delay())

    for churn_log_node_id in logdata.ChurnData.add_node_events:
        add_delay = logdata.ChurnData.add_node_events[churn_log_node_id]
        core.event_queue.create_event(events.AddNodeEvent, add_delay, churn_log_node_id)

    core.create_monitor_node()

    print("Starting simulation")
    start_sim_time = time.time()
    try:
        return core.run()
    except KeyboardInterrupt:
        pass
    finally:
        print("Finished simulation. dt={:f}".format(time.time() - start_sim_time))


if __name__ == '__main__':
    sys.exit(main(sys.argv))
