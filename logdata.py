import random
import re


class ChurnData:
    open_node_ids_sim_init = set()
    add_node_events = {}
    delete_node_events = {}

    @classmethod
    def process_log(cls, path):
        unixtime_sim_init = None
        line_nr = 0
        line_count = sum(1 for line in open(path))

        for line in open(path):
            logent = line.rstrip().split(';')
            logent_unixtime = int(logent[0])
            logent_node_id = logent[2]
            logent_event = logent[3]
            line_nr += 1

            if re.match('.*socket_send_error.*', line):
                # socket_send_errors are logged incorrectly ('closed' before 'established')
                # note that, in those cases, the socket is closed immediately after being opened
                # note also that this closed-reason happens very infrequently
                continue

            if logent_event.startswith('established'):
                if unixtime_sim_init is None:
                    cls.open_node_ids_sim_init.add(logent_node_id)
                else:
                    delay = logent_unixtime - unixtime_sim_init
                    cls.add_node_events[logent_node_id] = delay

            elif logent_event.startswith('closed'):
                if unixtime_sim_init is None:
                    cls.open_node_ids_sim_init.remove(logent_node_id)
                else:
                    assert logent_node_id in cls.add_node_events or logent_node_id in cls.open_node_ids_sim_init
                    delay = logent_unixtime - unixtime_sim_init
                    if logent_node_id not in cls.open_node_ids_sim_init:
                        delay -= cls.add_node_events[logent_node_id]
                    cls.delete_node_events[logent_node_id] = delay

            if line_nr > line_count / 2 and unixtime_sim_init is None:
                unixtime_sim_init = logent_unixtime


class LatencyData:
    latencies = []

    @classmethod
    def process_log(cls, path):
        ping_time = {}

        for line in open(path):
            logent = line.rstrip().split(';')

            logent_unixtime = int(logent[0])
            logent_node_id = logent[2]
            logent_type = logent[3]
            logent_nonce = logent[4]

            ping_time_key = logent_node_id + logent_nonce

            if logent_type == 'ping':
                ping_time[ping_time_key] = logent_unixtime
            elif logent_type == 'pong':
                if ping_time_key not in ping_time:
                    continue

                latency = logent_unixtime - ping_time[ping_time_key]
                cls.latencies.append(latency)

    @classmethod
    def get_random_latency(cls):
        return random.choice(cls.latencies)


class InvData:
    new_transaction_delays = []

    @classmethod
    def process_log(cls, path):
        known_hashes = set()
        last_new_transaction_timestamp = None

        for line in open(path):
            logent = line.rstrip().split(';')
            logent_unixtime = int(logent[0])
            logent_type = int(logent[3])
            logent_hash = logent[4]

            if logent_type != 1:  # 1 is transaction. ignore other inv-messages for now
                continue

            if logent_hash in known_hashes:
                continue

            known_hashes.add(logent_hash)

            if last_new_transaction_timestamp is not None:
                delay = logent_unixtime - last_new_transaction_timestamp
                assert delay >= 0
                cls.new_transaction_delays.append(delay)
            last_new_transaction_timestamp = logent_unixtime

    @classmethod
    def get_random_new_transaction_delay(cls):
        return random.choice(cls.new_transaction_delays)