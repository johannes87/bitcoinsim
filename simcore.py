from bintrees import FastAVLTree
import random
from enum import Enum
import events
import logdata
import params
import time


class Message(Enum):
    inv = events.InvReceivedEvent
    tx = events.TxReceivedEvent
    getdata = events.GetDataReceivedEvent


class InvType(Enum):
    tx = 1
    block = 2  # currently not used


class InvElem:
    def __init__(self, inv_hash, inv_type: InvType):
        self.inv_hash = inv_hash
        self.inv_type = inv_type


class Node:
    def __init__(self, core, node_id):
        self.core = core
        self.id = node_id
        self.outgoing = set()
        self.incoming = set()
        self.inv_hash_salt = Core.create_new_inv_hash()
        self.peers_inv_known = {}
        self.inv_to_send = {}
        self.tx_pool = set()

    def create_outgoing_connections(self):
        while len(self.outgoing) < params.node_max_outgoing:
            self.connect_to_random_node()

    def send_message(self, dest_node_id, message: Message, *args):
        self.core.event_queue.create_event(
            message.value,
            round(logdata.LatencyData.get_random_latency() / 2),
            self.id,
            dest_node_id,
            *args)

    def relay_transaction(self, inv_hash):
        for connected_node_id in self.outgoing.union(self.incoming):
            if self.peer_is_inv_known(connected_node_id, inv_hash):
                continue

            if connected_node_id not in self.inv_to_send:
                self.inv_to_send[connected_node_id] = []

            self.inv_to_send[connected_node_id].append(InvElem(inv_hash, InvType.tx))

    def connect_to(self, outgoing_node):
        self.outgoing.add(outgoing_node.id)
        outgoing_node.incoming.add(self.id)

    def reconnect_outgoing(self):
        self.connect_to_random_node()

    def connect_to_random_node(self):
        # NOTE: theoretically it could happen that this loop will never end, because no new node
        # can be found. However, with the amount of nodes (> 4000) we have, and because our assumption
        # is that the network size stays constant, this is not going to happen.
        # TODO: find a way to check this beforehand; maybe implement this without a loop
        outgoing_node = None
        while outgoing_node is None:
            random_node = self.core.node_map.get_random_node()
            if random_node is self:
                continue
            if random_node.id in self.incoming.union(self.outgoing):
                continue
            outgoing_node = random_node

        self.connect_to(outgoing_node)

    def peer_add_inv_known(self, node_id, inv_hash):
        if node_id not in self.peers_inv_known:
            self.peers_inv_known[node_id] = set()
        self.peers_inv_known[node_id].add(inv_hash)

    def peer_is_inv_known(self, node_id, inv_hash):
        if node_id not in self.peers_inv_known:
            return False
        else:
            return inv_hash in self.peers_inv_known[node_id]

    def add_transaction(self, inv_hash):
        self.tx_pool.add(inv_hash)

    def has_transaction(self, inv_hash):
        return inv_hash in self.tx_pool

    def inv_message_received(self, timestamp, src_node_id, inv_list):
        inv_list_getdata = []
        for inv_elem in inv_list:
            if inv_elem.inv_type != InvType.tx:
                continue

            self.peer_add_inv_known(src_node_id, inv_elem.inv_hash)
            if not self.has_transaction(inv_elem.inv_hash):
                inv_list_getdata.append(inv_elem)

        self.send_message(src_node_id, Message.getdata, inv_list_getdata)

    def getdata_message_received(self, timestamp, src_node_id, inv_list):
        for inv_elem in inv_list:
            if inv_elem.inv_type != InvType.tx:
                continue

            if self.has_transaction(inv_elem.inv_hash):
                self.send_message(src_node_id, Message.tx, inv_elem.inv_hash)

    def tx_message_received(self, timestamp, src_node_id, inv_hash):
        self.add_transaction(inv_hash)
        self.relay_transaction(inv_hash)


class MonitorNode(Node):
    """
    MonitorNode is a special node which has connections to all nodes
    Relaying of transactions is disabled to circumvent information eclipsing
    The purpose is to generate an inv.log similar to the modified bitcoin client
    """
    def __init__(self, core, node_id):
        super().__init__(core, node_id)
        self.inv_log_file = open('inv_simulated.log', 'w+')

    def create_outgoing_connections(self):
        # try to connect to all existing nodes
        for outgoing_node_id in self.core.node_map.get_node_ids():
            if outgoing_node_id not in self.incoming.union(self.outgoing) and \
                    outgoing_node_id != self.id:
                outgoing_node = self.core.node_map.get_node(outgoing_node_id)
                self.connect_to(outgoing_node)

    def reconnect_outgoing(self):
        # try to maintain all connections when a connection is lost
        # don't do it on every lost connection to increase performance
        if len(self.outgoing.union(self.incoming)) < \
                self.core.node_map.node_count() * params.monitor_node_reconnect_threshold:
            self.create_outgoing_connections()

    def inv_message_received(self, timestamp, src_node_id, inv_list):
        for inv_elem in inv_list:
            self.inv_log_file.write(";".join([
                str(timestamp),
                '0.0.0.0',  # no ip address information in simulator
                str(src_node_id),
                str(inv_elem.inv_type.value),
                str(inv_elem.inv_hash)
            ]) + '\n')

    def getdata_message_received(self, timestamp, src_node_id, inv_list):
        # ignore getdata messages
        pass

    def tx_message_received(self, timestamp, src_node_id, inv_hash):
        # ignore tx messages
        pass


class NodeMap:
    def __init__(self, core):
        self.core = core
        self.node_map = {}
        self.next_node_id = 1

    def get_node(self, node_id: int) -> Node:
        if node_id in self.node_map:
            return self.node_map[node_id]

    def get_node_ids(self):
        return self.node_map.keys()

    def get_random_node(self) -> Node:
        random_node_id = random.choice(list(self.node_map.keys()))
        return self.get_node(random_node_id)

    def add_node(self, node):
        self.node_map[node.id] = node

    def create_node(self, churn_log_node_id=None, node_cls=Node) -> Node:
        new_node = node_cls(self.core, self.next_node_id)
        self.next_node_id += 1
        self.add_node(new_node)

        self.core.event_queue.create_event(
            events.ThreadMessageHandlerLoopEvent,
            random.randint(0, params.thread_message_handler_delay),
            new_node.id
        )

        if churn_log_node_id is not None and churn_log_node_id in logdata.ChurnData.delete_node_events:
            delay = logdata.ChurnData.delete_node_events[churn_log_node_id]
            self.core.event_queue.create_event(events.DeleteNodeEvent, delay, new_node.id)

        return new_node

    def delete_node(self, node):
        for outgoing_node_id in node.outgoing:
            outgoing_node = self.get_node(outgoing_node_id)
            outgoing_node.incoming.remove(node.id)

        for incoming_node_id in node.incoming:
            incoming_node = self.get_node(incoming_node_id)
            incoming_node.outgoing.remove(node.id)

        del self.node_map[node.id]

    def node_exists(self, node_id):
        return node_id in self.node_map

    def node_count(self):
        return len(self.node_map)


class EventQueue:
    def __init__(self, core):
        self.data = FastAVLTree()
        self.cur_event = None
        self.core = core
        self.event_count = 0

    def add_event(self, event):
        assert event.core is self.core
        if event.timestamp not in self.data:
            self.data[event.timestamp] = []
        self.data[event.timestamp].append(event)
        self.event_count += 1

    def create_event(self, event_cls, delay, *args):
        if self.cur_event is None:
            timestamp = delay
        else:
            timestamp = self.cur_event.timestamp + delay

        new_event = event_cls(self.core, timestamp, *args)
        self.add_event(new_event)

    def run_event(self, event):
        assert self.cur_event is None or event.timestamp >= self.cur_event.timestamp
        self.cur_event = event
        event.run()

    def pop_next_event(self):
        assert self.data.count > 0
        (ts, event_list) = self.data.pop_min()
        assert len(event_list) > 0
        next_event = event_list.pop(0)
        if len(event_list) > 0:
            self.data[ts] = event_list
        self.event_count -= 1
        return next_event

    def count(self):
        return self.event_count


class Core:
    def __init__(self):
        self.node_map = NodeMap(self)
        self.event_queue = EventQueue(self)
        self.monitor_node = None

    @classmethod
    def create_new_inv_hash(cls):
        """
        create a representation for a new inventory hash
        inventory hashes are implemented as integers, with range [0, 2^256 - 1]
        """
        return random.randint(0, 2**256 - 1)

    def create_monitor_node(self):
        self.monitor_node = self.node_map.create_node(None, MonitorNode)

    def run_next_event(self):
        cur_event = self.event_queue.pop_next_event()
        self.event_queue.run_event(cur_event)
        return cur_event

    def run(self):
        i = 0
        while self.event_queue.count() > 0:
            start_event = time.time()
            last_event = self.run_next_event()
            end_event = time.time()
            if i % 5000 == 0:
                print("i={:d}, #nodes: {:d}, #events: {:d}, last_event: ts={:f}, event={:s}, dt={:f}".format(
                    i, self.node_map.node_count(),
                    self.event_queue.count(),
                    last_event.timestamp,
                    repr(last_event),
                    end_event - start_event
                ))
            i += 1
        return 0