#!/usr/bin/python3

import unittest
import simcore
import events
import params
import logdata
import random
from pprint import pprint
from bintrees import BinaryTree


class BaseTestCase(unittest.TestCase):
    def setUp(self):
        self.core = simcore.Core()

        for i in range(10000):
            logdata.LatencyData.latencies.append(random.randint(100, 500))
            logdata.InvData.new_transaction_delays.append(random.randint(10000, 20000))

        # try to run ReconnectOutgoingEvent as the next even after DeleteNodeEvent
        params.reconnect_delay = [0, 0]

    def check_node_map_conditions(self):
        for node_id in self.core.node_map.node_map:
            node = self.core.node_map.get_node(node_id)

            self.assertTrue(len(node.outgoing) == params.node_max_outgoing)

            for outgoing_node_id in node.outgoing:
                outgoing_node = self.core.node_map.get_node(outgoing_node_id)
                self.assertTrue(node.id in outgoing_node.incoming)

            for incoming_node_id in node.incoming:
                incoming_node = self.core.node_map.get_node(incoming_node_id)
                self.assertTrue(node.id in incoming_node.outgoing)

    def check_delete_node_event_count_invariant(self):
        """Each existing node has to have exactly one DeleteNodeEvent"""
        delete_node_events = [e for e in self.get_pending_events() if type(e) is events.DeleteNodeEvent]
        self.assertEqual(self.core.node_map.node_count(), len(delete_node_events))

    def run_next_event_of_type(self, event_cls):
        # copy necessary b/c undefined behaviour when modifying while iterating
        for ts in BinaryTree(self.core.event_queue.data):
            event_list = self.core.event_queue.data[ts]
            for event in event_list:
                if type(event) is event_cls:
                    self.core.event_queue.run_event(event)
                    event_list = [e for e in event_list if e is not event]
                    if len(event_list) > 0:
                        self.core.event_queue.data[ts] = event_list
                    else:
                        del self.core.event_queue.data[ts]
                    return

    def delete_events_of_type(self, event_cls):
        # copy necessary b/c undefined behaviour when modifying while iterating
        for ts in BinaryTree(self.core.event_queue.data):
            event_list = self.core.event_queue.data[ts]
            event_list = [e for e in event_list if type(e) is not event_cls]
            if len(event_list) > 0:
                self.core.event_queue.data[ts] = event_list
            else:
                del self.core.event_queue.data[ts]

    def get_pending_events(self):
        pending_events = []
        for ts in self.core.event_queue.data:
            event_list = self.core.event_queue.data[ts]
            for event in event_list:
                pending_events.append(event)
        return pending_events

    def fetch_all_nodes(self):
        node_list = []
        for node_id in self.core.node_map.node_map:
            node_list.append(self.core.node_map.get_node(node_id))
        return node_list


class TestCreateNodesEvent(BaseTestCase):
    def test_create_nodes(self):
        self.core.event_queue.create_event(events.CreateNodesEvent, 0, 100)
        self.core.run_next_event()
        self.check_node_map_conditions()
        self.assertEqual(self.core.node_map.node_count(), 100)

        self.core.event_queue.create_event(events.CreateNodesEvent, 0, 10)
        self.core.run_next_event()
        self.check_node_map_conditions()
        self.assertEqual(self.core.node_map.node_count(), 110)

        self.check_delete_node_event_count_invariant()


class TestDeleteAddNodeEvent(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.old_tmh_delay = params.thread_message_handler_delay
        # ensure TMH doesn't run here, could interfere
        params.thread_message_handler_delay = 10000000000000

    def tearDown(self):
        params.thread_message_handler_delay = self.old_tmh_delay

    def test_delete_nodes(self):
        self.core.event_queue.create_event(events.CreateNodesEvent, 0, 200)
        self.core.run_next_event()
        self.check_node_map_conditions()
        self.assertEqual(self.core.node_map.node_count(), 200)
        self.check_delete_node_event_count_invariant()

        # run a single DeleteNodeEvent
        self.core.run_next_event()
        self.check_delete_node_event_count_invariant()
        self.assertEqual(self.core.node_map.node_count(), 199)

        # NOTE: we assume here that the next events are the ReconnectOutgoingEvents created
        # by the last DeleteNodeEvent. This works because all the DeleteEvents are at least
        # 10000 us in the future (see setUp method), and the ReconnectOutgoingEvents are queued
        # as soon as possible

        # Run all ReconnectOutgoingEvents, after this check_node_map_conditions should be fine again
        while type(self.get_pending_events()[0]) is events.ReconnectOutgoingEvent:
            self.core.run_next_event()
        self.check_node_map_conditions()

        self.core.event_queue.create_event(events.AddNodeEvent, 0)
        self.core.run_next_event()  # runs only AddNodeEvent
        self.check_node_map_conditions()
        self.check_delete_node_event_count_invariant()
        self.assertEqual(self.core.node_map.node_count(), 200)

        # next event is AddNodeEvent again, created by last AddNodeEvent (see timestamp-ranges in setUp())
        self.core.run_next_event()
        self.check_node_map_conditions()
        self.check_delete_node_event_count_invariant()
        self.assertEqual(self.core.node_map.node_count(), 201)


class TestRelayTransactionNoTrickling(BaseTestCase):
    def test_relay_transaction_no_trickling(self):
        self.core.event_queue.create_event(events.CreateNodesEvent, 0, 200)
        self.core.run_next_event()
        self.check_node_map_conditions()

        # trickling false for:
        # hash_salt = 98405085567304743823245924032162186455689241283105763414354132998673973619462
        # inv_hash = 89359841651625565372181754116621769709490062858131559305757122139107440494190

        src_node = self.core.node_map.get_random_node()
        src_node.inv_hash_salt = 98405085567304743823245924032162186455689241283105763414354132998673973619462
        new_transaction_hash = 89359841651625565372181754116621769709490062858131559305757122139107440494190
        src_node.relay_transaction(new_transaction_hash)
        src_node.add_transaction(new_transaction_hash)

        self.core.event_queue.create_event(events.ThreadMessageHandlerLoopEvent, 0, src_node.id)
        self.core.run_next_event()

        inv_events = [e for e in self.get_pending_events() if type(e) is events.InvReceivedEvent]
        connected_nodes = src_node.outgoing.union(src_node.incoming)

        # no trickling, so there has to be a inventory event for each connected node
        self.assertEqual(len(inv_events), len(connected_nodes))

        # pprint([("ts " + str(e.timestamp), "src_id " + str(e.src_node_id), "dest_id " + str(e.dest_node_id), e) for e in inv_events])
        # print("#inv events", len(inv_events))


class TestRelayTransactionTrickling(BaseTestCase):
    def test_relay_transaction_no_trickling(self):
        self.core.event_queue.create_event(events.CreateNodesEvent, 0, 200)
        self.core.run_next_event()
        self.check_node_map_conditions()

        # delete TMHEvents generated by core.create_node(): not needed, and they interfere with testing
        # (b/c they are generated randomly, with minimum delay of 0)
        self.delete_events_of_type(events.ThreadMessageHandlerLoopEvent)

        # trickling true for:
        # hash_salt = 22432856069351975037612389974728365367484592562078732546057422988131137334250
        # inv_hash = 68503521093900826065168560242301184988737901392049023879931082355605433396647

        src_node = self.core.node_map.get_random_node()
        src_node.inv_hash_salt = 22432856069351975037612389974728365367484592562078732546057422988131137334250
        new_transaction_hash = 68503521093900826065168560242301184988737901392049023879931082355605433396647
        src_node.relay_transaction(new_transaction_hash)
        src_node.add_transaction(new_transaction_hash)

        self.core.event_queue.create_event(events.ThreadMessageHandlerLoopEvent, 0, src_node.id)
        self.core.run_next_event()

        inv_events = [e for e in self.get_pending_events() if type(e) is events.InvReceivedEvent]

        # each inventory is trickled, except for trickle_node, so there has to be exactly 1 inv event
        self.assertEqual(len(inv_events), 1)

        # next event is InvReceivedEvent (see timings in setUp, message events have highest priority)
        self.assertIsInstance(self.get_pending_events()[0], events.InvReceivedEvent)
        self.core.run_next_event()

        # GetDataReceivedEvent
        self.assertIsInstance(self.get_pending_events()[0], events.GetDataReceivedEvent)
        self.core.run_next_event()

        # TxReceivedEvent
        self.assertIsInstance(self.get_pending_events()[0], events.TxReceivedEvent)
        self.core.run_next_event()

        n_have_tx = 0
        # now there should be exactly two nodes which have the initial transaction
        for node in self.fetch_all_nodes():
            if node.has_transaction(new_transaction_hash):
                n_have_tx += 1

        self.assertEqual(n_have_tx, 2)


if __name__ == '__main__':
    unittest.main()
